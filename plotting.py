import device
import parameters

import matplotlib.pyplot as plt
import numpy as np
from typing import List

def convert_spikes_to_array(spikes: List[device.Spike]):
	'''
	Convert a list of spikes to an (N,2) np.ndarray, with the second column the neuron ids and first column 
	the spike timestamps in ms, normalised so the first timestamp is 0.
	'''
	spikes_array = np.vstack(([spike.timestamp for spike in spikes],[spike.id for spike in spikes])).T
	spikes_array[:,0] = (spikes_array[:,0] - spikes_array[0,0]) / 1000
	return spikes_array

def extract_spikes_group(spikes_array: np.ndarray, group: List[int]):
	'''
	Extract a subset of spikes from the spikes array - only those with ids in the group.
	'''
	spikes_grouped = np.vstack([spikes_array[spikes_array[:,1] == neuron_id,:] for neuron_id in group])
	spikes_grouped[:,1] = [group.index(id) for id in spikes_grouped[:,1]]
	return spikes_grouped

def plot_spikes(dynapse: device.DynapseDevice, spikes: List[device.Spike], show_params=True, filename_to_save: str=None, **figure_kwargs):
	'''
	Show a raster plot of the spikes from each of the dynapse's monitored neurons. Provide extra args to change the figure properties (eg. figsize).
	'''
	spikes_array = convert_spikes_to_array(spikes)

	fig, ax = plt.subplots(1,1, **figure_kwargs)
	# sort the spikes by neuron id, based on the groups of monitored neurons on the dynapse
	current_id = 0
	for group in dynapse.monitored_neuron_groups:
		spikes_grouped = extract_spikes_group(spikes_array, group)
		ax.plot(spikes_grouped[:,0], spikes_grouped[:,1] + current_id, '|')
		current_id += len(group)

	ax.set_xlabel('time (ms)')
	ax.set_ylabel('Neuron ID')
	if show_params:
		plt.title('\n'.join(parameters.get_non_default_params_str(dynapse.model)))

	plt.tight_layout()
	if filename_to_save is not None:
		plt.savefig(filename_to_save)
	plt.show()

def plot_spikes_and_frequencies(dynapse: device.DynapseDevice, spikes: List[device.Spike], show_params=True, filename_to_save: str=None, **figure_kwargs):
	'''
	Show a raster plot of the spikes from each of the dynapse's monitored neurons, as well as the average firing frequencies of each neuron. 
	Provide extra args to change the figure properties (eg. figsize).
	'''
	spikes_array = convert_spikes_to_array(spikes)

	fig, axs = plt.subplots(1,2, sharey=True, gridspec_kw={'width_ratios': [3,1]}, **figure_kwargs)
	# sort the spikes by neuron id, based on the groups of monitored neurons on the dynapse
	current_id = 0
	for group in dynapse.monitored_neuron_groups:
		spikes_grouped = extract_spikes_group(spikes_array, group)
		spike_rates_ms = np.array([np.count_nonzero(spikes_grouped[:,1] == neuron_index) for neuron_index in range(len(group))]) / (spikes_array[-1,0] - spikes_array[0,0]) * 1000
		axs[0].plot(spikes_grouped[:,0], spikes_grouped[:,1] + current_id, '|')
		axs[1].plot(spike_rates_ms, range(current_id,current_id+len(group)), '.')
		current_id += len(group)

	axs[0].set_xlabel('time (ms)')
	axs[0].set_ylabel('Neuron ID')
	axs[1].set_xlabel('Firing rate (Hz)')
	if show_params:
		plt.title('\n'.join(parameters.get_non_default_params_str(dynapse.model)), loc='center')

	plt.tight_layout()
	if filename_to_save is not None:
		plt.savefig(filename_to_save)
	plt.show()