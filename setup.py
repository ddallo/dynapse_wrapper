from setuptools import setup

setup(
	name='dynapse_wrapper',
	version='0.1.5',
	description='Wrapper for samna-dynapse1 libraries for ctxctl, making it easier to initalise and run network simulations.',
	author='Dominic Dall\'Osto',
	author_email='ddallo@ini.uzh.ch',
	install_requires=['ctxctl-contrib','numpy']
)