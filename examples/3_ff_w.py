import numpy as np

import parameters
import device
import network

# --------------- FF curve ----------------
# Connect a Poisson spike generator to a neuron
# Monitor the spikes of this neuron with different input frequencies
# given different AMPA weight parameters

# open DYNAP-SE1 board to get Dynapse1Model
dynapse = device.DynapseDevice()
model = dynapse.model
parameters.set_all_default_params(model)

# weight fine value list, given coarse value = 7
fine_values = np.arange(0, 50, 10)
fine_values = np.append(fine_values, np.arange(50, 300, 100))
coarse_value = 7
# the input rate values
input_freqs = np.arange(0, 200, 20)
# result frequency list
freqs = np.zeros((len(fine_values), len(input_freqs)))
# duration per input rate per w
duration = 1

# ------------------- build network -------------------
# select a spike generator
schip = 0
score = 0
sid = 1

# select a neuron
chip = 0
core = 1
nid = 16

net = network.DynapseNetworkGenerator()
spikegen = net.get_spikegen(schip, score, sid)
neuron = net.get_neuron(chip, core, nid)
net.add_connection(spikegen, neuron, network.SYNAPSE_AMPA)
model.apply_configuration(net.get_config())

# set up Poisson spike generator
poisson_gen = dynapse.get_poisson_spikegen(input_freqs[0], schip, score, sid)
# monitor neuron with graph
dynapse.monitor_neuron(chip, core, nid)

dynapse.start_graph()

# ----------- get events with different input rates different weight -----------
for i in range(len(fine_values)):
	# set new weight
	parameters.set_param(model, parameters.AMPA_WEIGHT, (coarse_value,fine_values[i]), chip, core)

	for j in range(len(input_freqs)):
		# set the new input rate
		poisson_gen.set_rate(input_freqs[j])
		# start poisson_gen
		poisson_gen.start()
		# ------------ get events -----------
		events = dynapse.run_simulation(duration)	
		# stop poisson_gen
		poisson_gen.stop()
		# append the frequency to the list
		freq = len(events)/duration
		freqs[i][j] = freq

	print(coarse_value, fine_values[i], list(freqs[i]))

dynapse.stop_graph()
dynapse.close()