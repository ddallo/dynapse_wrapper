import parameters
import device

# open DYNAP-SE1 board to get Dynapse1Model
dynapse = device.DynapseDevice()
model = dynapse.model

parameters.set_all_default_params(model)

# --------------- FI curve ----------------
# Monitor the spikes of a single neuron with different DC input

# prepare the DC values
# Note: don't not use too large coarse values (>3). Too many spikes will kill the board..
DC_values = [(0, 0), (0, 50), (0, 100), (0, 200), (1, 50), (1, 100), \
            (1, 200), (2, 50), (2, 100), (2, 200), (3, 50), (3, 100)]
# linear DC values, float instead of tuple
linear_DC_values = []
# result frequency list
freqs = []

# choose a neuron to monitor.
chip = 0
core = 1
nid = 16
dynapse.monitor_neuron(chip, core, nid)

dynapse.start_graph()

duration = 1
for dc in DC_values:
		# set new DC
		linear_dc_value = parameters.set_param(model, parameters.NEURON_DC_INPUT, dc, chip, core)
		linear_DC_values.append(linear_dc_value)

		# get events
		events = dynapse.run_simulation(duration)

		freqs.append(len(events)/duration)

# close Dynapse1
dynapse.stop_graph()
dynapse.close()

print(linear_DC_values)
print(freqs)