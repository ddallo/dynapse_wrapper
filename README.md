# Dynapse Wrapper

## Installing

First install the `samna-dynapse1-pip-installable` branch of `ctxctl_contrib` with pip.

```bash
git clone https://gitlab.com/neuroinf/ctxctl_contrib.git
cd ctxctl_contrib
git checkout samna-dynapse1-pip-installable
cd ctxctl_contrib
python3 -m pip install --editable ctxctl_contrib
```

Now install Dynapse Wrapper in the same way.

```bash
python3 -m pip install --editable <dynapse_wrapper-folder-location>
```

## Usage

Full example usage is shown in the `examples/` folder.

To open the device:

```python
import device
import parameters
import network
import plotting

dynapse = device.DynapseDevice()
model = dynapse.model
parameters.set_all_default_params(model)
```

To create a network:

```python
# init a network generator
net = network.DynapseNetworkGenerator()
# create spike generators
schip = 0
score = 0
sids = np.arange(5,10)
spikegens = net.get_spikegens(schip, score, sids)
# create neurons
chip = 0
core = 1
nids = np.arange(15,20)
neurons = net.get_neurons(chip, core, nids)
# connect spikegens to neurons (only in the network topology not chips for now)
net.add_connections_all_to_all(spikegens, neurons, network.SYNAPSE_AMPA)
```

To create a Poisson spike generator and graph for monitoring neuron spiking:

```python
rate = 10 # Hz
poisson_gens = dynapse.get_poisson_spikegens(rate, schip, score, sids)
dynapse.monitor_neuron_network(neurons)
dynapse.start_graph()
```

To change parameters:

```python
parameters.set_param(model, parameters.AMPA_WEIGHT, (3,50), chip, core)
```

To run a simulation and collect spiking events:

```python
duration = 2 # s
poisson_gens.start()
events = dynapse.run_simulation(duration)
poisson_gens.stop()
dynapse.stop_graph()
```

To plot simulation results:

```python
plotting.plot_spikes_and_frequencies(dynapse, spikes, figsize=(15,5))
```

When finished, close the dynapse:

```python
dynapse.close()
```